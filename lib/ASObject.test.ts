'use strict';
/// <reference types="jest" />
/// <reference types="rewire" />

import {ASObject, Link, ObjectType} from "./index";

declare var require: any;

const rewire = require('rewire');

const ASObject = rewire('../../dist/activityStreams/ASObject');
const objectRefType = ASObject.__get__('objectRefType');
const objectRefsType = ASObject.__get__('objectRefsType');

describe('objectRefType tests', () => {
  test('objectRefType detects strings', () => {
    expect(objectRefType('hello')).toBe('string')
  });

  test('objectRefType detects Links', () => {
    const link: Link = {
      type: 'Link'
    };
    expect(objectRefType(link)).toBe('Link');
  });

  test('objectRefType detects ASObjects', () => {
    const object: ASObject = {
      type: ObjectType.Note
    };
    expect(objectRefType(object)).toBe('ASObject');
  });
});

describe('objectRefsType tests', () => {
  test('objectRefsType handles arrays of strings appropriately', () => {
    expect(objectRefsType(['hello'])).toBe('array');
  });

  test('objectRefsType handles strings appropriately', () => {
    expect(objectRefsType('hello')).toBe('string');
  });

  test('objectRefsType detects Links', () => {
    const link: Link = {
      type: 'Link'
    };
    expect(objectRefsType(link)).toBe('Link');
  });

  test('objectRefsType detects ASObjects', () => {
    const object: ASObject = {
      type: ObjectType.Note
    };
    expect(objectRefsType(object)).toBe('ASObject');
  });
});
