export enum ObjectType {
    Note = 'Note',
    Document = 'Document',
    Relationship = 'Relationship',
    Article = 'Article',
    Event = 'Event',
    Place = 'Place',
    Mention = 'Mention',
    Profile = 'Profile',
    Tombstone = 'Tombstone',
    Audio = 'Audio',
    Image = 'Image',
    Video = 'Video',
    Page = 'Page',
    Actor = 'Actor',
    Application = 'Application',
    Group = 'Group',
    Organization = 'Organization',
    Person = 'Person',
    Service = 'Service',
    Activity = 'Activity',
    IntransitiveActivity = 'IntransitiveActivity',
    Accept = 'Accept',
    TentativeAccept = 'TentativeAccept',
    Add = 'Add',
    Arrive = 'Arrive',
    Create = 'Create',
    Delete = 'Delete',
    Follow = 'Follow',
    Ignore = 'Ignore',
    Join = 'Join',
    Leave = 'Leave',
    Offer = 'Offer',
    Invite = 'Invite',
    Reject = 'Reject',
    TentativeReject = 'TentativeReject',
    Remove = 'Remove',
    Undo = 'Undo',
    Update = 'Update',
    View = 'View',
    Listen = 'Listen',
    Read = 'Read',
    Move = 'Move',
    Travel = 'Travel',
    Announce = 'Announce',
    Block = 'Block',
    Flag = 'Flag',
    Like = 'Like',
    Dislike = 'Dislike',
    Question = 'Question',
    Collection = 'Collection',
    OrderedCollection = 'OrderedCollection',
    CollectionPage = 'CollectionPage',
    OrderedCollectionPage = 'OrderedCollectionPage'
}

/**
 * Describes an object of any kind. The Object type serves as the base type for most of the other kinds of objects
 * defined in the Activity Vocabulary, including other Core types such as Activity, IntransitiveActivity, Collection
 * and OrderedCollection.
 */
export interface ASObject {
    [propName: string]: any;

    type: ObjectType;

    id?: string;

    '@context'?: string | string[];

    /**
     * A simple, human-readable, plain-text name for the object. HTML markup MUST NOT be included. The name MAY be
     * expressed using multiple language-tagged values.
     */
    name?: string;
    nameMap?: { [name: string]: string };

    /**
     * Identifies a resource attached or related to an object that potentially requires special handling. The intent is
     * to provide a model that is at least semantically similar to attachments in email.
     */
    attachment?: ObjectRefs<ASObject>;

    /**
     * Identifies one or more entities to which this object is attributed. The attributed entities might not be Actors.
     * For instance, an object might be attributed to the completion of another activity.
     */
    attributedTo?: ObjectRefs<ASObject>;

    /**
     * Identifies one or more entities that represent the total population of entities for which the object can
     * considered to be relevant.
     */
    audience?: ObjectRefs<ASObject>;

    /**
     *  The content or textual representation of the Object encoded as a JSON string. By default, the value of content is
     *  HTML. The mediaType property can be used in the object to indicate a different content type.
     *
     *  The content MAY be expressed using multiple language-tagged values.
     */
    content?: string;
    contentMap?: { [name: string]: string };

    /**
     *  Identifies the context within which the object exists or an activity was performed.
     *
     *  The notion of "context" used is intentionally vague. The intended function is to serve as a means of grouping
     *  objects and activities that share a common originating context or purpose. An example could be all activities
     *  relating to a common project or event.
     */
    context?: any;

    /**
     * The date and time describing the actual or expected ending time of the object. When used with an Activity object,
     * for instance, the endTime property specifies the moment the activity concluded or is expected to conclude.
     */
    endTime?: string;

    /**
     *  Identifies the entity (e.g. an application) that generated the object.
     */
    generator?: any;

    /**
     * Indicates an entity that describes an icon for this object. The image should have an aspect ratio of one
     * (horizontal) to one (vertical) and should be suitable for presentation at a small size.
     */
    icon?: ObjectRef<ASObject>;

    /**
     *  An image document of any kind.
     */
    image?: Image;

    /**
     *  Indicates one or more entities for which this object is considered a response.
     */
    inReplyTo?: ObjectRef<Document>;

    /**
     *  Indicates one or more physical or logical locations associated with the object.
     */
    location?: ObjectRef<ASObject>;

    /**
     *  Identifies an entity that provides a preview of this object.
     */
    preview?: ObjectRef<ASObject>;

    /**
     *  The date and time at which the object was published.
     */
    published?: string;

    /**
     *  Identifies a Collection containing objects considered to be responses to this object.
     */
    replies?: Collection<ASObject> | string;

    /**
     *  The date and time describing the actual or expected starting time of the object. When used with an Activity
     *  object, for instance, the startTime property specifies the moment the activity began or is scheduled to begin.
     */
    startTime?: string;

    /**
     *  A natural language summarization of the object encoded as HTML. Multiple language tagged summaries MAY be
     *  provided.
     */
    summary?: string;
    summaryMap?: { [name: string]: string };

    /**
     * One or more "tags" that have been associated with an objects. A tag can be any kind of Object. The key difference
     * between attachment and tag is that the former implies association by inclusion, while the latter implies
     * associated by reference.
     */
    tag?: ObjectRefs<ASObject>;

    /**
     *  The date and time at which the object was updated
     */
    updated?: string;

    /**
     *  Identifies one or more links to representations of the object
     */
    url?: LinkRefs;

    /**
     *  Identifies an entity considered to be part of the public primary audience of an Object
     */
    to?: ObjectRefs<Actor | Collection<ASObject>>;

    /**
     *  Identifies an Object that is part of the private primary audience of this Object.
     */
    bto?: ObjectRefs<Actor | Collection<ASObject>>;

    /**
     *  Identifies an Object that is part of the public secondary audience of this Object.
     */
    cc?: ObjectRefs<Actor | Collection<ASObject>>;

    /**
     *  Identifies one or more Objects that are part of the private secondary audience of this Object.
     */
    bcc?: ObjectRefs<Actor | Collection<ASObject>>;

    /**
     *  When used on an Object, identifies the MIME media type of the value of the content property. If not specified,
     *  the content property is assumed to contain text/html content.
     */
    mediaType?: string;
}

export type ObjectRef<T extends ASObject> = T | LinkRef;
export type ObjectRefs<T extends ASObject> = ObjectRef<T> | [ ObjectRef<T> ];

/**
 *  Describes a relationship between two individuals. The subject and object properties are used to identify the
 *  connected individuals.
 */
export interface Relationship extends ASObject {
    type: ObjectType.Relationship;

    /**
     *  On a Relationship object, the subject property identifies one of the connected individuals. For instance, for a
     *  Relationship object describing "John is related to Sally", subject would refer to John.
     */
    subject?: ObjectRef<Actor>;

    /**
     *  When used within a Relationship describes the entity to which the subject is related.
     */
    object?: ObjectRef<Actor>;

    /**
     *   On a Relationship object, the relationship property identifies the kind of relationship that exists between
     *   subject and object.
     */
    relationship?: ASObject;
}

/**
 *  Represents any kind of multi-paragraph written work.
 */
export interface Article extends ASObject {
    type: ObjectType.Article;
}

type DocumentSubType =
    ObjectType.Audio |
    ObjectType.Image |
    ObjectType.Video |
    ObjectType.Page;

/**
 *  Represents a document of any kind.
 */
export interface Document extends ASObject {
    type: ObjectType.Document | DocumentSubType;
}

/**
 *  Represents an audio document of any kind.
 */
export interface Audio extends Document {
    type: ObjectType.Audio;
}

/**
 *  An image document of any kind
 */
export interface Image extends Document {
    type: ObjectType.Image;
}

/**
 *  Represents a video document of any kind.
 */
export interface Video extends Document {
    type: ObjectType.Video;
}

/**
 *  Represents a short written work typically less than a single paragraph in length.
 */
export interface Note extends ASObject {
    type: ObjectType.Note;
}

/**
 *  Represents a Web Page.
 */
export interface Page extends Document {
    type: ObjectType.Page;
}

/**
 *  Represents any kind of event.
 */
export interface Event extends ASObject {
    type: ObjectType.Event;
}

/**
 *  Represents a logical or physical location. See 5.3 Representing Places for additional information.
 */
export interface Place extends ASObject {
    type: ObjectType.Place;

    /**
     *  Indicates the accuracy of position coordinates on a Place objects. Expressed in properties of percentage. e.g.
     *  "94.0" means "94.0% accurate".
     */
    accuracy?: number;

    /**
     *  Indicates the altitude of a place. The measurement units is indicated using the units property. If units is not
     *  specified, the default is assumed to be "m" indicating meters.
     */
    altitude?: number;

    /**
     *  The latitude of a place
     */
    latitude?: number;

    /**
     *  The longitude of a place
     */
    longitude?: number;

    /**
     *  The radius from the given latitude and longitude for a Place. The units is expressed by the units property. If
     *  units is not specified, the default is assumed to be "m" indicating "meters".
     */
    radius?: number;

    /**
     *  Specifies the measurement units for the radius and altitude properties on a Place object. If not specified, the
     *  default is assumed to be "m" for "meters".
     *
     *  Range: "cm" | " feet" | " inches" | " km" | " m" | " miles" | xsd:anyURI
     */
    units?: string;
}

/**
 *  A specialized Link that represents an @mention.
 */
export interface Mention extends Link {
    type: ObjectType.Mention;
}

/**
 *  A Profile is a content object that describes another Object, typically used to describe Actor Type objects. The
 *  describes property is used to reference the object being described by the profile.
 */
export interface Profile extends ASObject {
    type: ObjectType.Profile;
    /**
     *  On a Profile object, the describes property identifies the object described by the Profile.
     */
    describes?: ASObject;
}

/**
 *  A Tombstone represents a content object that has been deleted. It can be used in Collections to signify that there
 *  used to be an object at this position, but it has been deleted.
 */
export interface Tombstone extends ASObject {
    type: ObjectType.Tombstone;

    /**
     *  On a Tombstone object, the formerType property identifies the type of the object that was deleted.
     */
    formerType?: string;

    /**
     *  On a Tombstone object, the deleted property is a timestamp for when the object was deleted.
     */
    deleted?: string;
}

export function objectRefType (objectRef: ObjectRef<any>): string {
    if (!objectRef) {
        return 'undefined';
    }
    if (typeof objectRef === 'string') {
        return 'string';
    }
    if (objectRef.type === 'Link') {
        return linkRefType(objectRef as LinkRef)
    }
    return 'ASObject';
}

export function objectRefsType (objectRefs: ObjectRefs<any>): string {
    if (Array.isArray(objectRefs)) {
        return 'array';
    }
    return objectRefType(objectRefs as ObjectRef<any>);
}

// compile-time type safety
export type ActivitySubType =
    ObjectType.Accept |
    ObjectType.TentativeAccept |
    ObjectType.Add |
    ObjectType.Arrive |
    ObjectType.Create |
    ObjectType.Delete |
    ObjectType.Follow |
    ObjectType.Ignore |
    ObjectType.Join |
    ObjectType.Leave |
    ObjectType.Offer |
    ObjectType.Invite |
    ObjectType.Reject |
    ObjectType.TentativeReject |
    ObjectType.Remove |
    ObjectType.Undo |
    ObjectType.Update |
    ObjectType.View |
    ObjectType.Listen |
    ObjectType.Read |
    ObjectType.Move |
    ObjectType.Travel |
    ObjectType.Announce |
    ObjectType.Block |
    ObjectType.Flag |
    ObjectType.Like |
    ObjectType.Dislike |
    ObjectType.Question;

// run-time types
export const ActivitySubTypes = [
    ObjectType.Accept,
    ObjectType.TentativeAccept,
    ObjectType.Add,
    ObjectType.Arrive,
    ObjectType.Create,
    ObjectType.Delete,
    ObjectType.Follow,
    ObjectType.Ignore,
    ObjectType.Join,
    ObjectType.Leave,
    ObjectType.Offer,
    ObjectType.Invite,
    ObjectType.Reject,
    ObjectType.TentativeReject,
    ObjectType.Remove,
    ObjectType.Undo,
    ObjectType.Update,
    ObjectType.View,
    ObjectType.Listen,
    ObjectType.Read,
    ObjectType.Move,
    ObjectType.Travel,
    ObjectType.Announce,
    ObjectType.Block,
    ObjectType.Flag,
    ObjectType.Like,
    ObjectType.Dislike,
    ObjectType.Question,
];

export type ActorRef = Actor | LinkRef;
export type ActorRefs = ActorRef | [ ActorRef ];

/**
 *  An Activity is a subtype of Object that describes some form of action that may happen, is currently happening, or
 *  has already happened. The Activity type itself serves as an abstract base type for all types of activities. It is
 *  important to note that the Activity type itself does not carry any specific semantics about the kind of action
 *  being taken.
 */
export interface Activity extends ASObject {
    type: ActivitySubType;
    /**
     *  Describes one or more entities that either performed or are expected to perform the activity. Any single activity
     *  can have multiple actors. The actor MAY be specified using an indirect Link.
     */
    actor?: ActorRefs;

    /**
     *  Describes an object of any kind. The Object type serves as the base type for most of the other kinds of objects
     *  defined in the Activity Vocabulary, including other Core types such as Activity, IntransitiveActivity, Collection
     *  and OrderedCollection.
     */
    object?: ObjectRef<ASObject>;

    /**
     *  Describes the indirect object, or target, of the activity. The precise meaning of the target is largely dependent
     *  on the type of action being described but will often be the object of the English preposition "to". For instance,
     *  in the activity "John added a movie to his wishlist", the target of the activity is John's wishlist. An activity
     *  can have more than one target.
     */
    target?: ObjectRef<ASObject>;

    /**
     *  Describes the result of the activity. For instance, if a particular action results in the creation of a new
     *  resource, the result property can be used to describe that new resource.
     */
    result?: ObjectRef<ASObject>;

    /**
     *  Describes an indirect object of the activity from which the activity is directed. The precise meaning of the
     *  origin is the object of the English preposition "from". For instance, in the activity "John moved an item to List
     *  B from List A", the origin of the activity is "List A".
     */
    origin?: ObjectRef<ASObject>;

    /**
     *  Identifies one or more objects used (or to be used) in the completion of an Activity.
     */
    instrument?: ObjectRef<ASObject>;

}

export type IntransitiveActivitySubTypes =
    ObjectType.Arrive |
    ObjectType.Travel |
    ObjectType.Question;

/**
 *  An Activity is a subtype of Object that describes some form of action that may happen, is currently happening, or
 *  has already happened. The Activity type itself serves as an abstract base type for all types of activities. It is
 *  important to note that the Activity type itself does not carry any specific semantics about the kind of action
 *  being taken.
 */
export interface IntransitiveActivity extends ASObject {
    type: IntransitiveActivitySubTypes;

    /**
     *  Describes one or more entities that either performed or are expected to perform the activity. Any single activity
     *  can have multiple actors. The actor MAY be specified using an indirect Link.
     */
    actor?: ActorRefs;

    /**
     *  Describes the indirect object, or target, of the activity. The precise meaning of the target is largely dependent
     *  on the type of action being described but will often be the object of the English preposition "to". For instance,
     *  in the activity "John added a movie to his wishlist", the target of the activity is John's wishlist. An activity
     *  can have more than one target.
     */
    target?: ObjectRef<ASObject>;

    /**
     *  Describes the result of the activity. For instance, if a particular action results in the creation of a new
     *  resource, the result property can be used to describe that new resource.
     */
    result?: ObjectRef<ASObject>;

    /**
     *  Describes an indirect object of the activity from which the activity is directed. The precise meaning of the
     *  origin is the object of the English preposition "from". For instance, in the activity "John moved an item to List
     *  B from List A", the origin of the activity is "List A".
     */
    origin?: ObjectRef<ASObject>;

    /**
     *  Identifies one or more objects used (or to be used) in the completion of an Activity.
     */
    instrument?: ObjectRef<ASObject>;
}

export type AcceptSubType = ObjectType.TentativeAccept;

/**
 *  Indicates that the actor accepts the object. The target property can be used in certain circumstances to indicate
 *  the context into which the object has been accepted.
 */
export interface Accept extends Activity {
    type: ObjectType.Accept | AcceptSubType;
}

/**
 *  A specialization of Accept indicating that the acceptance is tentative.
 */
export interface TentativeAccept extends Accept {
    type: ObjectType.TentativeAccept;
}

/**
 *  Indicates that the actor has added the object to the target. If the target property is not explicitly specified,
 *  the target would need to be determined implicitly by context. The origin can be used to identify the context from
 *  which the object originated.
 */
export interface Add extends Activity {
    type: ObjectType.Add;
}

/**
 *  An IntransitiveActivity that indicates that the actor has arrived at the location. The origin can be used to
 *  identify the context from which the actor originated. The target typically has no defined meaning.
 */
export interface Arrive extends IntransitiveActivity {
    type: ObjectType.Arrive;
}

/**
 * Indicates that the actor has created the object.
 */
export interface Create extends Activity {
    type: ObjectType.Create;
}

/**
 *  Indicates that the actor has deleted the object. If specified, the origin indicates the context from which the
 *  object was deleted.
 */
export interface Delete extends Activity {
    type: ObjectType.Delete;
    deleted: string;
    formerType: string;
}

/**
 *  Indicates that the actor is "following" the object. Following is defined in the sense typically used within Social
 *  systems in which the actor is interested in any activity performed by or on the object. The target and origin
 *  typically have no defined meaning.
 */
export interface Follow extends Activity {
    type: ObjectType.Follow;
}

/**
 *  Indicates that the actor is ignoring the object. The target and origin typically have no defined meaning.
 */
export interface Ignore extends Activity {
    type: ObjectType.Ignore;
}

/**
 *  Indicates that the actor has joined the object. The target and origin typically have no defined meaning.
 */
export interface Join extends Activity {
    type: ObjectType.Join;
}

/**
 *  Indicates that the actor has left the object. The target and origin typically have no meaning.
 */
export interface Leave extends Activity {
    type: ObjectType.Leave;
}

/**
 *  Indicates that the actor likes, recommends or endorses the object. The target and origin typically have no defined
 *  meaning.
 */
export interface Like extends Activity {
    type: ObjectType.Like;
}

type OfferSubType = ObjectType.Invite;

/**
 *  Indicates that the actor is offering the object. If specified, the target indicates the entity to which the object
 *  is being offered.
 */
export interface Offer extends Activity {
    type: ObjectType.Offer | OfferSubType;
}

/**
 *  A specialization of Offer in which the actor is extending an invitation for the object to the target.
 */
export interface Invite extends Offer {
    type: ObjectType.Invite;
}

type RejectSubType = ObjectType.TentativeReject;

/**
 *  Indicates that the actor is rejecting the object. The target and origin typically have no defined meaning.
 */
export interface Reject extends Activity {
    type: ObjectType.Reject | RejectSubType;
}

/**
 *  A specialization of Reject in which the rejection is considered tentative.
 */
export interface TentativeReject extends Reject {
    type: ObjectType.TentativeReject;
}

/**
 *  Indicates that the actor is removing the object. If specified, the origin indicates the context from which the
 *  object is being removed.
 */
export interface Remove extends Activity {
    type: ObjectType.Remove;
}

/**
 *  Indicates that the actor is undoing the object. In most cases, the object will be an Activity describing some
 *  previously performed action (for instance, a person may have previously "liked" an article but, for whatever
 *  reason, might choose to undo that like at some later point in time).
 *
 *  The target and origin typically have no defined meaning.
 */
export interface Undo extends Activity {
    type: ObjectType.Undo;
}

/**
 *  Indicates that the actor has updated the object. Note, however, that this vocabulary does not define a mechanism
 *  for describing the actual set of modifications made to object.
 *
 *  The target and origin typically have no defined meaning.
 */
export interface Update extends Activity {
    type: ObjectType.Update;
}

/**
 *  Indicates that the actor has viewed the object.
 */
export interface View extends Activity {
    type: ObjectType.View;
}

/**
 *  Indicates that the actor has listened to the object.
 */
export interface Listen extends Activity {
    type: ObjectType.Listen;
}

/**
 *  Indicates that the actor has read the object.
 */
export interface Read extends Activity {
    type: ObjectType.Read;
}

/**
 *  Indicates that the actor has moved object from origin to target. If the origin or target are not specified, either
 *  can be determined by context.
 */
export interface Move extends Activity {
    type: ObjectType.Move;
}

/**
 *  Indicates that the actor is traveling to target from origin. Travel is an IntransitiveObject whose actor specifies
 *  the direct object. If the target or origin are not specified, either can be determined by context.
 */
export interface Travel extends IntransitiveActivity {
    type: ObjectType.Travel;
}

/**
 *  Indicates that the actor is calling the target's attention the object.
 *
 *  The origin typically has no defined meaning.
 */
export interface Announce extends Activity {
    type: ObjectType.Announce;
}

/**
 *  Indicates that the actor is blocking the object. Blocking is a stronger form of Ignore. The typical use is to
 *  support social systems that allow one user to block activities or content of other users. The target and origin
 *  typically have no defined meaning.
 */
export interface Block extends Activity {
    type: ObjectType.Block;
}

/**
 *  Indicates that the actor is "flagging" the object. Flagging is defined in the sense common to many social platforms
 *  as reporting content as being inappropriate for any number of reasons.
 */
export interface Flag extends Activity {
    type: ObjectType.Flag;
}

/**
 *  Indicates that the actor dislikes the object.
 */
export interface Dislike extends Activity {
    type: ObjectType.Dislike;
}

/**
 *  Represents a question being asked. Question objects are an extension of IntransitiveActivity. That is, the Question
 *  object is an Activity, but the direct object is the question itself and therefore it would not contain an object
 *  property.
 *
 *  Either of the anyOf and oneOf properties MAY be used to express possible answers, but a Question object MUST NOT
 *  have both properties.
 */
export interface Question extends IntransitiveActivity {
    type: ObjectType.Question;
}

export type ActorSubType =
    ObjectType.Actor |
    ObjectType.Application |
    ObjectType.Group |
    ObjectType.Organization |
    ObjectType.Person |
    ObjectType.Service;

/**
 *  Describes one or more entities that either performed or are expected to perform the activity. Any single activity
 *  can have multiple actors. The actor MAY be specified using an indirect Link.
 */
export interface Actor extends ASObject {
    type: ObjectType.Actor | ActorSubType;
}

/**
 *  Describes a software application.
 */
export interface Application extends Actor {
    type: ObjectType.Application;
}

/**
 *   Represents a formal or informal collective of Actors.
 */
export interface Group extends Actor {
    type: ObjectType.Group;
}

/**
 *   Represents an organization.
 */
export interface Organization extends Actor {
    type: ObjectType.Organization;
}

/**
 *   Represents an individual person.
 */
export interface Person extends Actor {
    type: ObjectType.Person;
}

/**
 *   Represents a service of any kind.
 */
export interface Service extends Actor {
    type: ObjectType.Service;
}

export const ActorSubTypes = [
    'Actor',
    'Application',
    'Group',
    'Organization',
    'Person',
    'Service'
] as string[];

export function actorRefType(actorRef: ActorRef): string {
    if (ActorSubTypes.includes((actorRef as Actor).type)) {
        return "Actor";
    }
    return linkRefType(actorRef as LinkRef);
}

export function actorRefsType(actorRefs: ActorRefs): string {
    if (Array.isArray(actorRefs)) {
        return 'array';
    }
    return actorRefType(actorRefs);
}

type CollectionSubType =
    ObjectType.OrderedCollection |
    ObjectType.CollectionPage |
    ObjectType.OrderedCollectionPage;

export const CollectionSubTypes = [
    ObjectType.Collection,
    ObjectType.OrderedCollection,
    ObjectType.CollectionPage,
    ObjectType.OrderedCollectionPage
];

export type CollectionRef<T extends ASObject> = Collection<ASObject> | LinkRef;

/**
 *  A Collection is a subtype of Object that represents ordered or unordered sets of Object or Link instances.
 *
 *  Refer to the Activity Streams 2.0 Core specification for a complete description of the Collection type.
 */
export interface Collection<T extends ASObject> extends ASObject {
    type: ObjectType.Collection | CollectionSubType;
    /**
     *  A non-negative integer specifying the total number of objects contained by the logical view of the collection.
     *  This number might not reflect the actual number of items serialized within the Collection object instance.
     */
    totalItems?: number;

    /**
     *  In a paged Collection, indicates the page that contains the most recently updated member items.
     */
    current?: CollectionPageRef<T>;

    /**
     *  In a paged Collection, indicates the furthest preceeding page of items in the collection.
     */
    first?: CollectionPageRef<T>;

    /**
     *  In a paged Collection, indicates the furthest proceeding page of the collection.
     */
    last?: CollectionPageRef<T>;

    items?: ObjectRefs<T>;
}

export type CollectionPageRef<T extends ASObject> = CollectionPage<T> | LinkRef;

/**
 *  A subtype of Collection in which members of the logical collection are assumed to always be strictly ordered.
 */
export interface OrderedCollection<T extends ASObject> extends Collection<T> {
    type: ObjectType.OrderedCollection | ObjectType.OrderedCollectionPage;
    orderedItems: ObjectRefs<T>;
}

/**
 *  Used to represent distinct subsets of items from a Collection. Refer to the Activity Streams 2.0 Core for a
 *  complete description of the CollectionPage object.
 */
export interface CollectionPage<T extends ASObject> extends Collection<T> {
    type: ObjectType.CollectionPage | ObjectType.OrderedCollectionPage;

    /**
     *  Identifies the Collection to which a CollectionPage objects items belong.
     */
    partOf?: CollectionPageRef<T>;

    /**
     *  In a paged Collection, indicates the next page of items.
     */
    next?: CollectionPageRef<T>;

    /**
     *  In a paged Collection, identifies the previous page of items.
     */
    prev?: CollectionPageRef<T>;
}

/**
 Used to represent ordered subsets of items from an OrderedCollection. Refer to the Activity Streams 2.0 Core for a
 complete description of the OrderedCollectionPage object.
 */
export interface OrderedCollectionPage<T extends ASObject> extends OrderedCollection<T>, CollectionPage<T> {
    type: ObjectType.OrderedCollectionPage;
    /**
     *  A non-negative integer value identifying the relative position within the logical view of a strictly ordered
     *  collection.
     */
    startIndex?: number;
}

export function collectionPageRefType (collectionPageRef: CollectionPageRef<any>): string {
    if (typeof collectionPageRef === 'string') {
        return 'string';
    }
    if (collectionPageRef.type === 'Link') {
        return linkRefType(collectionPageRef as LinkRef);
    }
    return (collectionPageRef as Collection<any>).type;
}

/**
 * A Link is an indirect, qualified reference to a resource identified by a URL. The fundamental model for links is
 * established by [ RFC5988]. Many of the properties defined by the Activity Vocabulary allow values that are either
 * instances of Object or Link. When a Link is used, it establishes a qualified relation connecting the subject
 * (the containing object) to the resource identified by the href. Properties of the Link are properties of the
 * reference as opposed to properties of the resource.
 */
export interface Link {
    type: 'Link' | 'Mention';

    /**
     *  The target resource pointed to by a Link.
     */
    href?: string;

    /**
     * A link relation associated with a Link. The value MUST conform to both the [HTML5] and [RFC5988] "link relation"
     * definitions.
     * In the [HTML5], any string not containing the "space" U+0020, "tab" (U+0009), "LF" (U+000A), "FF" (U+000C),
     * "CR" (U+000D) or "," (U+002C) characters can be used as a valid link relation.
     */
    rel?: string | [ string ];

    /**
     *  When used on a Link, identifies the MIME media type of the referenced resource.
     */
    mediaType?: string;

    /**
     *  A simple, human-readable, plain-text name for the object. HTML markup MUST NOT be included. The name MAY be
     *  expressed using multiple language-tagged values.
     */
    name?: string;
    nameMap?: Map<string, string>;

    /**
     *  Hints as to the language used by the target resource. Value MUST be a [BCP47] Language-Tag.
     */
    hreflang?: string;

    /**
     *  On a Link, specifies a hint as to the rendering height in device-independent pixels of the linked resource.
     */
    height?: number;

    /**
     *  On a Link, specifies a hint as to the rendering width in device-independent pixels of the linked resource.
     */
    width?: number;

    /**
     *  Identifies an entity that provides a preview of this object.
     */
    preview?: Link | ASObject;
}

export type LinkRef = Link | string;
export type LinkRefs = LinkRef | LinkRef[];

export function linkRefType (linkRef: LinkRef): string {
    if (typeof linkRef === 'string') {
        return 'string'
    }
    return (linkRef as Link).type;
}

export function resolveLinkRef (linkRef: LinkRef): string | undefined {
    switch (linkRefType(linkRef)) {
        case 'string':
            return linkRef as string;
        case 'Link':
        case 'Mention':
            return (linkRef as Link).href;
    }
    return undefined;
}

export function resolveActorRefs (actorRefs: ActorRefs): string | undefined | (string | undefined)[] {
    if (actorRefsType(actorRefs) === 'array') {
        return (actorRefs as ActorRef[]).map((actorRef: ActorRef) => resolveObjectRef(actorRef));
    }
    return resolveObjectRef(actorRefs as ActorRef);
}

export function resolveObjectRef (objectRef: ObjectRef<any>): string | undefined {
    switch (objectRefType(objectRef)) {
        case 'string':
            if (objectRef.length > 1024) {
                throw new Error(`invalid objectRef: ${objectRef}`);
            }
            return objectRef as string;
        case 'Link':
        case 'Mention':
            return (objectRef as Link).href;
        case 'ASObject':
            return (objectRef as ASObject).id;
    }
    return undefined;
}

export function resolveObjectRefs (objectRefs: ObjectRefs<any>): string | string[] | undefined {
    if (objectRefsType(objectRefs) === 'array') {
        const objectRefsArray = objectRefs as ObjectRef<any>[];
        const result: string[] = [];
        objectRefsArray.forEach((objectRef: ObjectRef<any>) => {
            const items = resolveObjectRef(objectRef);
            if (items) {
                result.push(items)
            }
        });
        return result;
    }
    return resolveObjectRef(objectRefs as ObjectRef<any>);
}

export function addToObjectRefs <T extends ASObject> (objectRefs: ObjectRefs<T>, objectRef: ObjectRef<T>): ObjectRefs<T> {
    const id: string | undefined = resolveObjectRef(objectRef);
    if (id) {
        switch (objectRefsType(objectRefs)) {
            case 'array':
                (objectRefs as Array<ObjectRef<T>>).push(id);
                return objectRefs;
            case 'ASObject':
                // @ts-ignore
                return [(objectRefs as ASObject).id, id];
            case 'string':
                // @ts-ignore
                return [objectRefs as string, id];
        }
    }
    return objectRefs;
}

export function resolveCollectionPageRef (collectionPageRef: CollectionPageRef<any>): string | undefined {
    switch (collectionPageRefType(collectionPageRef)) {
        case 'string':
            return collectionPageRef as string;
        case 'Link':
        case 'Mention':
            return (collectionPageRef as Link).href;
        case 'Collection':
        case 'CollectionPage':
        case 'OrderedCollection':
        case 'OrderedCollectionPage':
            return (collectionPageRef as Collection<any>).id;
    }
}

export function resolveActorRef (actorRef: ActorRef): string | undefined {
    if (actorRef) {
        switch (actorRefType(actorRef)) {
            case 'string':
                return actorRef as string;
            case 'Link':
                return (actorRef as Link).href;
            case 'Actor':
                return (actorRef as Actor).id;
        }
    }
    return undefined;
}

