'use strict';
/// <reference types="jest" />
/// <reference types="rewire" />

import {Link} from "./Link";

declare var require: any;

const rewire = require('rewire');

const Link = rewire('../../dist/activityStreams/Link');

test('linkRefType works for strings', () => {
  expect(Link.linkRefType('hello')).toBe('string');
});

test('linkRefType works for links', () => {
  const link: Link = {
    type: "Link"
  };

  expect(Link.linkRefType(link)).toBe('Link');
});
